package com.ali.flashlight

import android.content.Context
import android.hardware.camera2.CameraManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {
    var state = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Dexter.withContext(this).withPermission(android.Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                @RequiresApi(Build.VERSION_CODES.M)
                override fun onPermissionGranted(p0: PermissionGrantedResponse?) {
                    runFleshLight()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: PermissionRequest?,
                    p1: PermissionToken?
                ) {

                }

                override fun onPermissionDenied(p0: PermissionDeniedResponse?) {
                    Toast.makeText(
                        this@MainActivity,
                        "Camera permision required",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            }).check()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun runFleshLight() {

        imagetbtn.setOnClickListener {
            if (!state) {
                val cameraManager: CameraManager =
                    getSystemService(Context.CAMERA_SERVICE) as CameraManager

                val cameraId = cameraManager.cameraIdList[0]
                cameraManager.setTorchMode(cameraId, true)
                state = true
                imagetbtn.setImageResource(R.drawable.torch_on)
            }else{
                val cameraManager: CameraManager =
                    getSystemService(Context.CAMERA_SERVICE) as CameraManager

                val cameraId = cameraManager.cameraIdList[0]
                cameraManager.setTorchMode(cameraId, false)
                state = false
                imagetbtn.setImageResource(R.drawable.torch_off)
            }
        }
    }
}